require('./index.css');
require('../common/nav-simple/index.js');

let _mm = require('Util/mm');

let _user = require('../../service/user-service.js');


var formError = {
    show: function (errMsg) {
        $('.error-item').show().find('.err-msg').text(errMsg);
    },
    hide: function () {
        $('.error-item').hide();
    }
};

//page 逻辑部分
let page = {
    init: function () {
        this.bindEvent();
    },
    // bindEvent() {
    //
    // }
    bindEvent: function () {
        var _this = this;
        //登录按钮的点击
        $('#submit').click(function () {
            _this.submit();
        });
        //如果按回车, 也是提交
        $('.user-content').keyup(function (e) {
            if (e.keyCode === 13) {
                _this.submit();
            }

        });

    },

    submit: function () {
        var formData = {
            username: $.trim($('#username').val()),
            password: $.trim($('#password').val()),
        };

        validateResult = this.validateForm(formData);

        if (validateResult.status) {
            // 验证成功
            _user.login(formData, function (res) {
                window.location.href = _mm.getUrlParam('redirect') || './index.html';

            }, function (errMsg) {
                formError.show(errMsg);

            });
        }
        // 验证失败
        else {
            // 错误提示
            formError.show(validateResult.msg);

        }

    },
    formValidate: function (formData) {
        let result = {
            status: false,
            msg: ''
        };
        if (_mm.validate(formData.username, 'require')) {
            result.msg = "用户名不能为空"
        }
        if (_mm.validate(formData.password, 'require')) {
            result.msg = "密码不能为空"
        }
        result.status = true;
        result.msg = '验证通过';
        return result;
    }
};

$(function () {
    page.init()

});

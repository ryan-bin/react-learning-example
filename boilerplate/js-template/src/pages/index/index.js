/*
* @Author: shendengnian
* @Date:   2017-07-25 06:27:57
* @Last Modified by:   shendengnian
* @Last Modified time: 2017-08-07 07:00:12
*/

'use strict';
require('./index.css');
require('../common/nav/index.js');
require('../common/header/index.js');
require('Util/slider/index.js');
var templateBanner = require('./banner.string');
var _mm = require('Util/mm.js');

$(function(){ 
	//渲染banner的html
	var bannerHtml = _mm.renderHtml(templateBanner);
	$('.banner-con').html(bannerHtml);
	var $slider = $('.banner').unslider({
	//初始化banner	
	dots: true
	});
	//前一张和后一张操作的事件绑定
	$('.banner-con .banner-arrow').click(function(){
		var forward = $(this).hasClass('prev') ? 'prev' : 'next';
		$slider.data('unslider')[forward]();
	});
});	

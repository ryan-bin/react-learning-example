
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const path = require('path');
const pkg = require('../../package.json');

const ENV = process.env.NODE_ENV || 'development';
const VERSION = `${pkg.version}`;
const devMode =ENV !== 'production'

module.exports = {
    entry: {
        'main': ['./src/page/main/index.js'],
        'index': ['./src/page/index/index.js']
    },
    output: {
        path: path.join(__dirname, 'dist'),//打包后的文件存放的地方
        publicPath: '/dist',
        filename: 'js/[name].js'//打包后输出文件的文件名, [name].js 目的就是跟酒entry 多个文件的时候，可以
    },
    externals: {
        //externals就是webpack可以不处理应用的某些依赖库，使用externals配置后，依旧可以在代码中通过CMD、AMD或者window/global全局的方式访问。
        'jquery': 'window.jQuery'
    },
    module: {
        rules: [
            {
                test: /\.js?$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader',
            },
            {
                test: /\.(css|scss|sass)$/,
                // 不分离的写法
                // use: ["style-loader", "css-loader",sass-loader"]
                // 使用postcss不分离的写法
                // use: ["style-loader", "css-loader", sass-loader","postcss-loader"]
                // 此处为分离css的写法
                /*use: extractTextPlugin.extract({
                    fallback: "style-loader",
                    use: ["css-loader", "sass-loader"],
                    // css中的基础路径
                    publicPath: "../"
                })*/
                // 此处为使用postcss分离css的写法
                use: extractTextPlugin.extract({
                    fallback: "style-loader",
                    use: ["css-loader", "sass-loader", "postcss-loader"],
                    // css中的基础路径
                    publicPath: "../"

                })
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: "[name].css",
            chunkFilename: "[id].css"
        })
    ],
    // 具体查看：https://webpack.js.org/plugins/split-chunks-plugin/#optimization-splitchunks
    optimization: {
    	splitChunks: {
    		cacheGroups: {
    			vendor: {
    				// test: /\.js$/,
    				test: path.resolve(__dirname, '../node_modules'),
    				chunks: "initial", //表示显示块的范围，有三个可选值：initial(初始块)、async(按需加载块)、all(全部块)，默认为all;
    				name: "vendor", //拆分出来块的名字(Chunk Names)，默认由块名和hash值自动生成；
    				minChunks: 1,
    				reuseExistingChunk: true,
    				enforce: true
    			}
    		}
    	}
    },

    //  参考：https://github.com/zhouyupeng/webpack4.x_demo
    // webpack-server-dev 配置
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        publicPath: '/',
        host: "127.0.0.1",
        port: "8089",
        overlay: true, // 浏览器页面上显示错误
        // open: true, // 开启自动打开浏览器
        // stats: "errors-only", //stats: "errors-only"表示只打印错误：
        hot: true // 开启热更新
    }
}

const path = require('path');
const webpack = require("webpack");
const merge = require("webpack-merge");
const webpackConfigBase = require('./webpack.base.conf');


const webpackConfigDev = {

    //webpack-dev-server 配置请访问：https://webpack.js.org/guides/development/#using-webpack-dev-server

    mode: 'development', // 通过 mode 声明开发环境

    output: {
        filename: 'js/[name].bundle.js',
        publicPath: '/dist',
        path: path.resolve(__dirname, '../dist')
    },
    // output: {
    //     path: path.resolve(__dirname, '../dist'),
    //     publicPath: '',
    //     filename: '../js/[name].bundle.js'
    // },
    // output: {
    //     path: path.resolve(__dirname, '../dist'),
    //     publicPath : '/dist',
    //     // filename: 'js/[name].bundle.js'
    //     filename: 'js/[name].bundle.js',
    // },
    devServer: {
        contentBase: path.join(__dirname, "../dist"),
        publicPath: '',
        // host: "127.0.0.1",
        port: "8089",
        overlay: true, // 浏览器页面上显示错误
        open: true, // 开启浏览器
        // stats: "errors-only", //stats: "errors-only"表示只打印错误：
        // hot: true, // 开启热更新
        hotOnly: true
        // 设置代理访问：https://webpack.js.org/configuration/dev-server/#devserver-proxy
    },

    plugins: [
        //热更新
        new webpack.HotModuleReplacementPlugin(),
    ],
    optimization: {
        runtimeChunk: false,
        splitChunks: {
            cacheGroups: {
                default: false,
                common: {

                    test: path.join(__dirname, "../src/pages/common"),
                    // filename : 'js/base.js',
                    // test: path.resolve(__dirname, '../src/pages/common'),
                    chunks: "all",
                    name: "common", //生成文件名，依据output规则
                    minChunks: 2,
                    maxInitialRequests: 5,
                    minSize: 0,
                    priority: 1
                }
            }
        }
    },
    devtool: 'inline-source-map',
}
module.exports = merge(webpackConfigBase, webpackConfigDev);
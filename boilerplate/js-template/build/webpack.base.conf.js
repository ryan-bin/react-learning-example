const path = require('path');
const webpack = require("webpack");
const glob = require("glob");
// 分离css

//消除冗余的css
const purifyCssWebpack = require("purifycss-webpack");
// html模板
const HtmlWebpackPlugin = require('html-webpack-plugin');
//静态资源输出
const copyWebpackPlugin = require("copy-webpack-plugin");
const rules = require("./webpack.rules.conf.js");
// 获取html-webpack-plugin参数的方法
var getHtmlConfig = function (name, title) {
    // https://github.com/jantimon/html-webpack-plugin
    return {
        // template: `./src/pages/${name}/index.html`,
        // // filename: `${name}.html`,//生成的html存放路径，相对于publicPath
        // filename: 'view/' + name + '.html',
        template: './src/view/' + name + '.html',// 指定产出的模板
        filename: `view/${name}.html`,  // 产出的文件名
        title: title,// 可以给模板设置变量名，在html模板中调用 htmlWebpackPlugin.options.title 可以使用
        // favicon: './favicon.ico',
        inject: true,
        hash: true, //开启hash  ?[hash]
        chunks: ['common', name], // 在产出的HTML文件里引入哪些代码块
        minify: process.env.NODE_ENV === "development" ? false : {
            minifyJS: true,
            removeComments: true, //移除HTML中的注释
            collapseWhitespace: true, //折叠空白区域 也就是压缩代码
            removeAttributeQuotes: true, //去除属性引用
        },
    };
};


module.exports = {
    entry: {
        // 多入口文件
        'common': ['./src/pages/common/index.js'],
        'index': ['./src/pages/index/index.js'],
        'list': ['./src/pages/list/index.js'],
        'user-login': ['./src/pages/user-login/index.js'],
        'user-register': ['./src/pages/user-register/index.js'],
        'user-pass-reset': ['./src/pages/user-pass-reset/index.js'],
        'user-center': ['./src/pages/user-center/index.js'],
        'user-center-update': ['./src/pages/user-center-update/index.js'],
        'user-pass-update': ['./src/pages/user-pass-update/index.js'],
        'result': ['./src/pages/result/index.js'],
        // common: path.resolve(__dirname + '/../src/pages/common/index.js'),
        // index: path.resolve(__dirname + '/../src/pages/index/index.js'),
        // user-login: path.resolve(__dirname + '/../src/pages/user-login/index.js'),
        // index: '../src/pages/index/index.js',
        // user-login: '../src/pages/user-login/index.js',
    },
    module: {
        rules: [...rules]
    },
    //将外部变量或者模块加载进来
    externals: {
        'jquery': 'window.jQuery'
    },
    resolve: {
        // 访问：https://webpack.js.org/configuration/resolve/
        alias: {
            Node_modules: path.resolve(__dirname + '../node_modules/'),
            Util: path.resolve(__dirname, '../src/util/'),
            Pages: path.resolve(__dirname + '../src/pages/'),
            common: path.resolve(__dirname + '../src/pages/common/'),
            Service: path.resolve(__dirname + '../src/service/'),
            Styles: path.resolve(__dirname + '../src/styles/'),
            Image: path.resolve(__dirname + '../src/assets/image/'),
            mm$: path.resolve(__dirname, '../src/util/mm.js')
        }
    },
    plugins: [
        // 全局暴露统一入口
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            'window.jQuery': 'jquery',
        }),
        //静态资源输出
        new copyWebpackPlugin([{
            from: path.resolve(__dirname, "../src/assets"),
            to: './assets',
            ignore: ['.*']
        }]),
        // 消除冗余的css代码
        new purifyCssWebpack({
            paths: glob.sync(path.join(__dirname, "../src/pages/*/*.html"))
        }),
        new HtmlWebpackPlugin(getHtmlConfig('index', '首页')),
        new HtmlWebpackPlugin(getHtmlConfig('user-login', '用户登录')),
        new HtmlWebpackPlugin(getHtmlConfig('user-register', '用户注册')),
        new HtmlWebpackPlugin(getHtmlConfig('user-pass-reset', '找回密码')),
        new HtmlWebpackPlugin(getHtmlConfig('user-center-update', '用户中心更新')),
        new HtmlWebpackPlugin(getHtmlConfig('user-pass-update', '用户中心更新')),
        new HtmlWebpackPlugin(getHtmlConfig('user-center', '用户中心')),
        new HtmlWebpackPlugin(getHtmlConfig('result', '操作结果')),

    ],

    optimization: {
        runtimeChunk: false,
        splitChunks: {
            cacheGroups: {
                default:false,
                common: {

                    // test: /[\\/]node_modules[\\/]/,
                    // filename : 'js/base.js',
                    // test: path.resolve(__dirname, '../src/pages/common'),
                    chunks: "all",
                    name: "common", //生成文件名，依据output规则
                    minChunks: 2,
                    maxInitialRequests: 5,
                    minSize: 0,
                    priority: 1
                }
            }
        }
    }




};

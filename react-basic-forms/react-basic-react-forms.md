#  react-basic-react-forms

本篇内容：

* [ ] 了解react中Forms

纯HTML表单：

```
<form>
  <label>
    Name:
    <input type="text" name="name" />
  </label>
  <input type="submit" value="Submit" />
</form>
```
当用户提交表单时，此表单具有浏览到新页面的默认HTML表单行为。如果您想在React中使用此行为，它就可以正常工作。但在大多数情况下，使用JavaScript函数来处理表单的提交并访问用户在表单中输入的数据是很方便的。实现这一目标的标准方法是使用一种称为“受控组件”的技术。

## 受控组件(Controlled Components)
在 HTML 中，表单元素如 <input>，<textarea> 和 <select> 表单元素通常保持自己的状态，并根据用户输入进行更新。而在 React 中，可变状态一般保存在组件的 state(状态) 属性中，并且只能通过 [setState()](https://reactjs.org/docs/react-component.html#setstate) 更新。

我们可以通过使 React 的 state 成为 “单一数据源原则” 来结合这两个形式。然后渲染表单的 React 组件也可以控制在用户输入之后的行为。这种形式，其值由 React 控制的输入表单元素称为“受控组件”。

例如，如果我们想使上一个例子在提交时记录名称，我们可以将表单写为受控组件：

```
class NameForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    alert('A name was submitted: ' + this.state.value);
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Name:
          <input type="text" value={this.state.value} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}
```

设置表单元素的 value 属性之后，其显示值将由 this.state.value 决定，以满足 React 状态的同一数据理念。每次键盘敲击之后会执行 handleChange 方法以更新 React 状态，显示值也将随着用户的输入改变。

由于 value 属性设置在我们的表单元素上，显示的值总是 this.state.value，以满足 state 状态的同一数据理念。由于 handleChange 在每次敲击键盘时运行，以更新 React state(状态)，显示的值将更新为用户的输入。

对于受控组件来说，每一次 state(状态) 变化都会伴有相关联的处理函数。这使得可以直接修改或验证用户的输入。比如，如果我们希望强制 name 的输入都是大写字母，可以这样来写 handleChange 方法：

```
handleChange(event) {
  this.setState({value: event.target.value.toUpperCase()});
}
```

## textare 标签
在 HTML 中，<textarea> 元素通过它的子节点定义了它的文本值：

```
<textarea>
  Hello there, this is some text in a text area
</textarea>
```

在 React 中，<textarea> 的赋值使用 value 属性替代。这样一来，表单中 <textarea> 的书写方式接近于单行文本输入框 ：

```
class EssayForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 'Please write an essay about your favorite DOM element.'
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    alert('An essay was submitted: ' + this.state.value);
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Essay:
          <textarea value={this.state.value} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}
```

注意，this.state.value 在构造函数中初始化，所以这些文本一开始就出现在文本域中。

## select 标签
在 HTML 中，<select> 创建了一个下拉列表。例如，这段 HTML 创建一个下拉的口味(flavors)列表：

```
<select>
  <option value="grapefruit">Grapefruit</option>
  <option value="lime">Lime</option>
  <option selected value="coconut">Coconut</option>
  <option value="mango">Mango</option>
</select>
```
注意，Coconut 选项是初始化选中的，因为它的 selected 属性。React 中，并不使用这个 selected 属性，而是在根 select 标签中使用了一个 value 属性。这使得受控组件使用更方便，因为你只需要更新一处即可。例如：

```
class FlavorForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: 'coconut'};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    alert('Your favorite flavor is: ' + this.state.value);
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Pick your favorite flavor:
          <select value={this.state.value} onChange={this.handleChange}>
            <option value="grapefruit">Grapefruit</option>
            <option value="lime">Lime</option>
            <option value="coconut">Coconut</option>
            <option value="mango">Mango</option>
          </select>
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}
```
总的来说，这使 <input type="text">， <textarea> 和 <select> 都以类似的方式工作 —— 它们都接受一个 value 属性可以用来实现一个受控组件。

注意
您可以将数组传递给value属性，允许您在select标记中选择多个选项：

```
<select multiple={true} value={['B', 'C']}>
```

## 文件输入标签
在HTML中，<input type="file">允许用户从其设备存储中选择一个或多个文件，以便上传到服务器或通过[File API](https://developer.mozilla.org/en-US/docs/Web/API/File/Using_files_from_web_applications)通过JavaScript进行操作。

```
<input type="file" />
```
因为它的值是只读的，所以它是React中一个不受控制的组件。它将在[后面文档](https://reactjs.org/docs/uncontrolled-components.html#the-file-input-tag)与其他不受控制的组件一起讨论。

## 处理多个输入

当您需要处理多个受控input元素时，可以name为每个元素添加一个属性，让处理程序函数根据值选择要执行的操作event.target.name。

例如：
```
class Reservation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isGoing: true,
      numberOfGuests: 2
    };

    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <form>
        <label>
          Is going:
          <input
            name="isGoing"
            type="checkbox"
            checked={this.state.isGoing}
            onChange={this.handleInputChange} />
        </label>
        <br />
        <label>
          Number of guests:
          <input
            name="numberOfGuests"
            type="number"
            value={this.state.numberOfGuests}
            onChange={this.handleInputChange} />
        </label>
      </form>
    );
  }
}
```
[在CodePen上试一试](https://codepen.io/gaearon/pen/wgedvV?editors=0010)

请注意我们如何使用ES6 [计算属性名称](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Operators/Object_initializer#Computed_property_names)语法来更新与给定输入名称对应的状态键：

```
this.setState({
  [name]: value
});
```

它相当于这个ES5代码：

```
var partialState = {};
partialState[name] = value;
this.setState(partialState);
```

此外，由于setState()自动[将部分状态合并到当前状态](https://reactjs.org/docs/state-and-lifecycle.html#state-updates-are-merged)，我们只需要使用更改的部分调用它。  

## 受控输入空值

在[受控组件](https://reactjs.org/docs/forms.html#controlled-components)上指定值prop会阻止用户更改输入，除非您需要。如果您已指定a value但输入仍可编辑，则可能意外设置value为undefined或null。
以下代码演示了这一点。（输入首先被锁定，但在短暂延迟后变为可编辑。）

```
ReactDOM.render(<input value="hi" />, mountNode);

setTimeout(function() {
  ReactDOM.render(<input value={null} />, mountNode);
}, 1000);
```

## 受控组件的替代品
使用受控组件有时会很繁琐，因为您需要为数据可以更改的每种方式编写事件处理程序，并通过React组件管理所有输入状态。当您将预先存在的代码库转换为React或将React应用程序与非React库集成时，这会变得特别烦人。在这些情况下，您可能希望检查[不受控制的组件](https://reactjs.org/docs/uncontrolled-components.html)，这是实现输入表单的替代技术







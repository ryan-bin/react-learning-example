# React-Redux-hello-world

本篇内容：

* [ ] 理解[官方案例](https://reacttraining.com/react-router/web/example/basic)


我们在[react-basic-jsx](./react-basic-jsx)的基础上，把`index.js`内容改为如下：
同时加入需要的包：

```javascript
yarn add -D webpack webpack-dev-server
yarn add -D react-router-dom
```

`index.js`

```
import React from "react";
import ReactDOM from 'react-dom'
import {BrowserRouter as Router, Route, Link} from "react-router-dom";

const BasicExample = () => (
    <Router>
        <div>
            <ul>
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/about">About</Link>
                </li>
                <li>
                    <Link to="/topics">Topics</Link>
                </li>
            </ul>

            <hr/>

            <Route exact path="/" component={Home}/>
            <Route path="/about" component={About}/>
            <Route path="/topics" component={Topics}/>
        </div>
    </Router>
);

const Home = () => (
    <div>
        <h2>Home</h2>
    </div>
);

const About = () => (
    <div>
        <h2>About</h2>
    </div>
);

const Topics = ({match}) => (
    <div>
        <h2>Topics</h2>
        <ul>
            <li>
                <Link to={`${match.url}/rendering`}>Rendering with React</Link>
            </li>
            <li>
                <Link to={`${match.url}/components`}>Components</Link>
            </li>
            <li>
                <Link to={`${match.url}/props-v-state`}>Props v. State</Link>
            </li>
        </ul>

        <Route path={`${match.url}/:topicId`} component={Topic}/>
        <Route
            exact
            path={match.url}
            render={() => <h3>Please select a topic.</h3>}
        />
    </div>
);

const Topic = ({match}) => (
    <div>
        <h3>{match.params.topicId}</h3>
    </div>
);


ReactDOM.render(
    <BasicExample/>,
    document.getElementById('root')
);
```


然后启动`yarn server`, 得到如下：

![](media/15315796022213/15315803427376.jpg)


流程如下：

1. BasicExample 渲染 div#root
2. 这个组件通过`Router`包含`Link`和`Route`,其中`Link`给页面提供链接路由，`Route`对链接的路由转向某个组件
3. 组件`Topics`,有一个`match`参数，而且这个组件中又包含`Link`和`Route`, 而且通过`${match.url}`获得上一个路由的基础路由，然后直接







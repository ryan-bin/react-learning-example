#  react-basic-react-handling-events


本篇内容：

* [ ] 了解react处理事件

> * React事件使用camelCase而不是小写命名。
> * 使用JSX，您可以将函数作为事件处理程序而不是字符串传递。

例如，HTML：

```javascipt
<button onclick="activateLasers()">
  Activate Lasers
</button>
```

在React中略有不同：

```javascipt
<button onClick={activateLasers}>
  Activate Lasers
</button>
```

另一个区别是您无法返回false以防止React中的默认行为。你必须使用preventDefault来明确。
例如，使用纯HTML，为了防止打开新页面的默认链接行为，您可以编写：

```javascipt
<a href="#" onclick="console.log('The link was clicked.'); return false">
  Click me
</a>
```
在react中：

```javascipt
function ActionLink() {
  function handleClick(e) {
    e.preventDefault();
    console.log('The link was clicked.');
  }

  return (
    <a href="#" onClick={handleClick}>
      Click me
    </a>
  );
}
```
这e是一个合成事件。React根据[W3C规范](https://www.w3.org/TR/DOM-Level-3-Events/)定义了这些合成事件，因此您无需担心跨浏览器兼容性。请参阅[SyntheticEvent](https://reactjs.org/docs/events.html)参考指南以了解更多信息。


使用React时，通常不需要addEventListener在创建DOM元素后调用它来添加侦听器。相反，只需在最初呈现元素时提供侦听器

使用[ES6](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Classes)类定义组件时，常见模式是将事件处理程序作为类的方法。例如，此Toggle组件呈现一个按钮，允许用户在“ON”和“OFF”状态之间切换：

```javascript
class Toggle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isToggleOn: true};

    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(prevState => ({
      isToggleOn: !prevState.isToggleOn
    }));
  }

  render() {
    return (
      <button onClick={this.handleClick}>
        {this.state.isToggleOn ? 'ON' : 'OFF'}
      </button>
    );
  }
}

ReactDOM.render(
  <Toggle />,
  document.getElementById('root')
);
```

您必须小心this在JSX回调的含义。在JavaScript中，默认情况下不会绑定类方法。如果你忘了绑定this.handleClick，并将它传递给onClick，this将undefined在函数实际上是调用。

这不是特定于React的行为; 它是[JavaScript中函数如何工作](https://www.smashingmagazine.com/2014/01/understanding-javascript-function-prototype-bind/)的一部分。通常，如果您引用一个没有()它的方法，例如onClick={this.handleClick}，您应该绑定该方法。

如果调用bind让你烦恼，有两种方法可以解决这个问题。如果您使用的是实验性的[公共类字段语法](https://babeljs.io/docs/plugins/transform-class-properties/)，则可以使用类字段正确绑定回调：

```javascript
class LoggingButton extends React.Component {
  // This syntax ensures `this` is bound within handleClick.
  // Warning: this is *experimental* syntax.
  handleClick = () => {
    console.log('this is:', this);
  }

  render() {
    return (
      <button onClick={this.handleClick}>
        Click me
      </button>
    );
  }
}
```

默认情况下，在[Create React App](https://github.com/facebookincubator/create-react-app)中启用此语法

如果您没有使用类字段语法，则可以在回调中使用[箭头函数](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Functions/Arrow_functions)

```javascript
class LoggingButton extends React.Component {
  handleClick() {
    console.log('this is:', this);
  }

  render() {
    // This syntax ensures `this` is bound within handleClick
    return (
      <button onClick={(e) => this.handleClick(e)}>
        Click me
      </button>
    );
  }
}
```
此语法的问题是每次`LoggingButton`渲染时都会创建不同的回调。在大多数情况下，这很好。但是，如果将此回调作为prop传递给较低组件，则这些组件可能会执行额外的重新呈现。我们通常建议在构造函数中使用绑定或使用类字段语法来避免这种性能问题。

## 将参数传递给事件处理程序

在循环内部，通常需要将额外的参数传递给事件处理程序。例如，如果id是行ID，则以下任何一个都可以工作：

```
<button onClick={(e) => this.deleteRow(id, e)}>Delete Row</button>

<button onClick={this.deleteRow.bind(this, id)}>Delete Row</button>
```
上面两行是等价的，并分别使用[箭头功能](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions)和[Function.prototype.bind](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_objects/Function/bind)。

在这两种情况下，e表示React事件的参数将作为ID之后的第二个参数传递。使用箭头函数，我们必须显式传递它，但bind任何进一步的参数都会自动转发



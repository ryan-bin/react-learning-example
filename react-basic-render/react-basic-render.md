# react-basic-render

本篇内容：

* [ ] 了解ReactDOM.render语法

ReactDOM.render主要用于元素渲染到DOM中，语法如下：

```
ReactDOM.render(reactElement, domContainerNode)
```

其中`reactElement`是你用react 写好的元素，`domContainerNode`是在html 中已经存在的元素，通过`document.getElementById('root')`等拿dom点的方式，定位渲染那个元素。


## 实验 
在react-basic-jsx 的基础上，项目目录名字react-basic-jsx改为react-basic-render，同时`package.json`中的name 改成：react-basic-render

同时把`index.js` 变化为如下：

```javascript
import React from 'react';
import ReactDOM from 'react-dom';

function tick() {
    const element = (
        <div>
            <h1>Hello, world!</h1>
            <h2>It is {new Date().toLocaleTimeString()}.</h2>
        </div>
    );
    ReactDOM.render(element, document.getElementById('root'));
}

setInterval(tick, 1000);
```

```
yarn add -D webpack webpack-dev-server  // 需要重新安装一下webpack

yarn start
```

打开index.html，得到：
![](media/15310417716613/15310426511869.jpg)





关于ReactDOM.render的工作原理，可以访问[How-does-ReactDOM-render-work](https://www.quora.com/How-does-ReactDOM-render-work)




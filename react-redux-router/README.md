# React-Router 中Router

前端路由：用户在浏览器端输入的url和网页的对应关系。简单来说就是用户在浏览器中输入url,在页面上可以显示什么。
更多关于浏览器对于路由这方面的认识，推荐访问[链接](https://github.com/kaola-fed/blog/issues/137)

### 原始网页路由方式
我们访问[Windows](https://developer.mozilla.org/en-US/docs/Web/API/Window)的用法。
发现大概有以下三种可以更改页面的方式：

```js
Window.open()
打开一个新窗口。


Window.location 只读
获取、设置window对象的location, 或者当前的 URL.

Window.history 只读
返回history对象的引用.
```
从`Window.open()`开始实验：
1. 首先在当前页面打开`Console`, 然后输入：`window.open('http://github.com')`, 发现浏览器会从新打开一个页面，这个页面就是`http://github.com`
2. 在`http://github.com`这个页面端继续打开`Console`,然后输入:`window.location = "#ry"`,发现我们的的浏览器没有打开任何页面或者跳转到其他地方,然而url 却变为：`https://github.com/#ry`,这就所说的我们页面路由的hash值变化了。我们可以直接设定一个监控事件,监听到hash值的变化，从而响应不同路径的逻辑处理:`window.addEventListener("hashchange", funcRef, false)`,或者如下方式：

```
window.onhashchange = function(){
    console.log('current hash:', window.location.hash);
}
```
* 接着我们输入:`window.location = "ry"`,然后跳转到：`https://github.com/ry`这个页面了,其实我们的`window.location`还有其他更多参数,具体访问[链接](https://developer.mozilla.org/zh-CN/docs/Web/API/Window/location)

3. 浏览器窗口有一个history对象，用来保存浏览历史,比如`window.history.length`就是得到当前窗口先后访问的页面数,更多请访问[API](https://developer.mozilla.org/en-US/docs/Web/API/History)

```js
History.back():
返回上一页

History.forward()：
去下一页

History.go():
从会话历史记录加载页面，该页面由其与当前页面的相对位置标识，例如前一页为-1或下一页为1。如果指定了越界值（例如，在会话历史记录中没有先前访问过的页面时指定-1），则此方法将无效地生效

History.pushState():
将给定数据推送到具有指定标题的会话历史堆栈，如果提供，则推送到URL。DOM将数据视为不透明; 您可以指定任何可以序列化的JavaScript对象

History.replaceState():
更新历史堆栈上的最新条目以获取指定的数据，标题以及URL（如果提供）。DOM将数据视为不透明; 您可以指定任何可以序列化的JavaScript对象
```
现在我们来具体查看[History.pushState()](https://developer.mozilla.org/en-US/docs/Web/API/History_API#The_pushState()_method)和[History.replaceState()](https://developer.mozilla.org/en-US/docs/Web/API/History_API#The_pushState()_method):
可以看出更改`history`并不会导致url的跳转，只是把url的一些信息存储到histroy 中,比如`history.pushState(null, null, 'https://www.baidu.com/33323');`, 在当前的baidu页面的`Console`中`window.history.length`就一直在增，而`History.replaceState()`只是替代之前的所以`window.history.length`的值不会变化。


### React-Router中Router

`react-router-dom `提供 `<BrowserRouter>` 和 `<HashRouter>` 两个路由组件。两者都提供一个`history`对象。通常来说如果你有一个服务端的返回就用`<BrowserRouter>`，如果您使用的是静态文件那么就用`<HashRouter>`,现在我们来做一些实验

`index.js`:

```js
import ReactDOM from 'react-dom';
import React from 'react'
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom'

const Home = () => (
    <div>
        <h2>Home</h2>
    </div>
)

const About = () => (
    <div>
        <h2>About</h2>
    </div>
)

const Topic = ({ match }) => (
    <div>
        <h3>{match.params.topicId}</h3>
    </div>
)

const Topics = ({ match }) => (
    <div>
        <h2>Topics</h2>
        <ul>
            <li>
                <Link to={`${match.url}/rendering`}>
                    Rendering with React
                </Link>
            </li>
            <li>
                <Link to={`${match.url}/components`}>
                    Components
                </Link>
            </li>
            <li>
                <Link to={`${match.url}/props-v-state`}>
                    Props v. State
                </Link>
            </li>
        </ul>

        <Route path={`${match.path}/:topicId`} component={Topic}/>
        <Route exact path={match.path} render={() => (
            <h3>Please select a topic.</h3>
        )}/>
    </div>
)

const BasicExample = () => (
    <Router>
        <div>
            <ul>
                <li><Link to="/">Home</Link></li>
                <li><Link to="/about">About</Link></li>
                <li><Link to="/topics">Topics</Link></li>
            </ul>

            <hr/>

            <Route exact path="/" component={Home}/>
            <Route path="/about" component={About}/>
            <Route path="/topics" component={Topics}/>
        </div>
    </Router>
)

ReactDOM.render(
    <BasicExample/>,
    document.getElementById('root')
);
```

```
yarn server
```

![](media/15317228144159/15317305133067.jpg)

然后我们直接在浏览器中输入:`http://localhost:8080/topics`,会发现报错：

![](media/15317228144159/15317306011256.jpg)

```
```

同时我们把`BrowserRouter`修改为`HashRouter`

![](media/15317228144159/15317304323999.jpg)

然后我们直接在浏览器中输入:`http://localhost:8080/#/topics`,会发现不会报错：

![](media/15317228144159/15317306534269.jpg)

#### BrowserRouter 对象 API

一个<Router>使用HTML5 history API（ pushState，replaceState和popstate事件），让您的UI与URL进行同步。

```js
<BrowserRouter
  basename={optionalString}
  forceRefresh={optionalBool}
  getUserConfirmation={optionalFunc}
  keyLength={optionalNumber}
>
	<App/>
</BrowserRouter>
```

`basename: string`:The base URL for all locations,如果您的应用程序是从服务器上的子目录提供的，则需要将其设置为子目录。格式正确的基本名称应该有一个前导斜杠，但没有尾部斜杠。

```js
<BrowserRouter basename="/calendar"/>
<Link to="/today"/> // renders <a href="/calendar/today">
```

`getUserConfirmation：func`:用于确认导航的功能。默认使用window.confirm。

```
// this is the default behavior
const getConfirmation = (message, callback) => {
  const allowTransition = window.confirm(message)
  callback(allowTransition)
}
<BrowserRouter getUserConfirmation={getConfirmation}/>
```

`forceRefresh：bool`:如果true路由器将在页面导航上使用整页刷新。您可能只想在不支持HTML5历史记录API的浏览器中使用它。

```js
const supportsHistory = 'pushState' in window.history
<BrowserRouter forceRefresh={!supportsHistory}/>
```

`keyLength: number`:The length of location.key. Defaults to 6.

```js
<BrowserRouter keyLength={12}/>
```

`children: node`:A [single child element](https://facebook.github.io/react/docs/react-api.html#react.children.only) to render.

#### HashRouter 对象 API
A <Router> that uses the hash portion of the URL (i.e. window.location.hash) to keep your UI in sync with the URL.

注意点：

```
IMPORTANT NOTE: Hash history does not support location.key or location.state. In previous versions we attempted to shim the behavior but there were edge-cases we couldn’t solve. Any code or plugin that needs this behavior won’t work. As this technique is only intended to support legacy browsers, we encourage you to configure your server to work with <BrowserHistory> instead.
```

```js
import { HashRouter } from 'react-router-dom'

<HashRouter>
  <App/>
</HashRouter>
```

`basename: string`:The base URL for all locations,格式正确的基本名称应该有一个前导斜杠，但没有尾部斜杠。

```js
<HashRouter basename="/calendar"/>
<Link to="/today"/> // renders <a href="#/calendar/today">
```

`getUserConfirmation：func`:用于确认导航的功能。默认使用window.confirm。

```js
//this is the default behavior
const getConfirmation = (message, callback) => {
  const allowTransition = window.confirm(message)
  callback(allowTransition)
}

<HashRouter getUserConfirmation={getConfirmation}/>
```

`hashType：string`:要用于的编码类型window.location.hash。可用值包括：

* "slash"- 创建像#/和的哈希#/sunshine/lollipops
* "noslash"- 创建像#和的哈希#sunshine/lollipops
* "hashbang"- 创建“ajax crawlable”（谷歌弃用）哈希像#!/和#!/sunshine/lollipops

默认为"slash"。

`children: node`:A [single child element](https://facebook.github.io/react/docs/react-api.html#react.children.only) to render.

小结：
1. 官方推荐用`<BrowserRouter>`而不用`HashRouter`
2. `BrowserRouter` 是用`history`实现的，`HashRouter`是用`window.location.hash`来实现的













参考：

https://developer.mozilla.org/en-US/docs/Web/API/History_API


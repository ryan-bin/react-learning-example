# react-basic-react-components-and-props

本篇内容：

* [ ] 了解组件和组件输入参数语法

通过组件(components)，您可以将UI拆分为独立的，可重复使用的部分，并单独考虑每个部分,props是component 中输入的参数

## 函数(Functional)和类(class)组件
组件可以两种形式，函数和类

```javascipt
function Welcome(props) {
  return <h1>Hello, {props.name}</h1>;
}
```

此函数是一个有效的React组件，因为它接受单个“props”（代表属性）对象参数与​​数据并返回一个React元素。我们将这些组件称为“功能”，因为它们实际上是JavaScript函数.还可以使用ES6类来定义组件：

```javascipt
class Welcome extends React.Component {
  render() {
    return <h1>Hello, {this.props.name}</h1>;
  }
}
```

`同时记住组件的名字一定要大写开头`

## 组件组合

1. 两者是并行关系
因为一个组件如果有多个元素必须用一个HTML tag包裹，比如这样才是合法的(用一个div包裹多个)

```javascript
function App() {
  return (
    <div>
      <Welcome name="Sara" />
      <Welcome name="Cahal" />
      <Welcome name="Edite" />
    </div>
  );
}

```

2. 子父关系

可以对一个大的组件进行分解成多个小组件，如下

```javascript
function Comment(props) {
  return (
    <div className="Comment">
      <div className="UserInfo">
        <img className="Avatar"
          src={props.author.avatarUrl}
          alt={props.author.name}
        />
        <div className="UserInfo-name">
          {props.author.name}
        </div>
      </div>
      <div className="Comment-text">
        {props.text}
      </div>
      <div className="Comment-date">
        {formatDate(props.date)}
      </div>
    </div>
  );
}
```

可以拆解如下：

```javascript
function formatDate(date) {
    return date.toLocaleDateString();
}

function Avatar(propss) {
    return (
        <img
            className="Avatar"
            src={propss.user.avatarUrl}
            alt={propss.user.name}
        />
    );
}

function UserInfo(props) {
    return (
        <div className="UserInfo">
            <Avatar user={props.user} />
            <div className="UserInfo-name">{props.user.name}</div>
        </div>
    );
}

function Comment(props) {
    return (
        <div className="Comment">
            <UserInfo user={props.author} />
            <div className="Comment-text">{props.text}</div>
            <div className="Comment-date">
                {formatDate(props.date)}
            </div>
        </div>
    );
}


```

```javascript
function formatDate(date) {
    return date.toLocaleDateString();
}

function Avatar(propss) {
    return (
        <img
            className="Avatar"
            src={propss.user.avatarUrl}
            alt={propss.user.name}
        />
    );
}
function UserInfo(props) {
    return (
        <div className="UserInfo">
            <Avatar user={props.user} />
            <div className="UserInfo-name">{props.user.name}</div>
        </div>
    );
}
function Comment(props) {
    return (
        <div className="Comment">
            <UserInfo user={props.author} />
            <div className="Comment-text">{props.text}</div>
            <div className="Comment-date">
                {formatDate(props.date)}
            </div>
        </div>
    );
}
const comment = {
    date: new Date(),
    text: 'I hope you enjoy learning React!',
    author: {
        name: 'Hello Kitty',
        avatarUrl: 'http://placekitten.com/g/64/64',
    },
};
ReactDOM.render(
    <Comment
        date={comment.date}
        text={comment.text}
        author={comment.author}
    />,
    document.getElementById('root')
);
```
现在我们来看一下参数是怎么输入的：
![](media/15310607166228/15311013764652.jpg)

`Comment`组件包含 `UserInfo`组件, `UserInfo`组件包含 `Avatar`组件, 

`Comment` 有三个参数：date, text, author, `UserInfo`有两个参数：user, user.name, `Avatar`有两个参数：user.avatarUrl, user.name.

如下：
![](media/15310607166228/15311017897127.jpg)

author作为一个json中的json,在UserInfo中，author 赋值给了 user 
![](media/15310607166228/15311018970994.jpg)
那么在`UserInfo`这个组件中的数据信息就变成了：

```json
    user: {
        name: 'Hello Kitty',
        avatarUrl: 'http://placekitten.com/g/64/64',
    }
```
所以在UserInfo组件中的信息必须通过`props.user`和·`{props.user.name}`来获得.






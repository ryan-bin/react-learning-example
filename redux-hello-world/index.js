import React from 'react'
import ReactDOM from 'react-dom'
import { createStore } from 'redux'
import Counter from './src/components/Counter'
import counter from './src/reducers/index'

//Store 是整个应用保存数据的地方，来自于redux中的createStore进行创建一个store
//createStore 接受一个函数, 返回一个新的store 对象， 这个store 包含着state, 通过Action 对state
// 的改变得到一个一个的view
const store = createStore(counter)
const rootEl = document.getElementById('root')

const render = () => ReactDOM.render(
  <Counter
    value={store.getState()}
    onIncrement={() => store.dispatch({ type: 'INCREMENT' })}
    onDecrement={() => store.dispatch({ type: 'DECREMENT' })}
  />,
  rootEl
)

render()
store.subscribe(render)

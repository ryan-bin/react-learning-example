# React-Redux 中link 和 NavLink

下面一步一步来展示`React-Redux` 的基本用法。
首先安装`react-router-dom`,它的作用主要用于DOM 绑定的 React Router

```
yarn add -D react-router-dom
```

### Link

我们在[react-basic-jsx](./react-basic-jsx)的基础上，把`index.js`内容改为如下：

```javascript
import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Link} from 'react-router-dom'


class Index extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
                {this.props.children}
                <Link to="/page1">页面1</Link>
                <br/>
                <Link to="/page2">页面2</Link>
                <br/>
                <Link to="/page3">页面3</Link>
            </div>
        )
    }
}
ReactDOM.render(
    <Router>
        <Index>

        </Index>
    </Router>,
    document.getElementById('root')
);
```
然后运行：`yarn server`, 得到如下页面：

![](media/15315489426581/15315511952316.jpg)

点击访问`页面1`，然后url 后面就多了一个page1, 变化为如下图：

![](media/15315489426581/15315574998016.jpg)

现在我们直接在浏览器中输入: `http://localhost:8080/page1`时就变为如下：

![](media/15315489426581/15315576915956.jpg)

*总结:*

> 用Link 触发的链接，只能从页面中点击过去，而在浏览器中输入这个url 时寻找不到的.
 
官方关于Link的API,访问[文档](https://reacttraining.com/react-router/web/api/Link)


### NavLink

A special version of the <Link> that will add styling attributes to the rendered element when it matches the current URL.
我们把`index.js` 中关于page2 的链接修改如下，其他不变：

```javascript
class Index extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
                {this.props.children}
                <Link to="/page1">页面1</Link>
                <br/>
                <NavLink
                    to="/page2"
                    activeStyle={{
                        fontWeight: 'bold',
                        color: 'red'
                    }}
                >页面2</NavLink>
                <br/>
                <Link to="/page3">页面3</Link>
            </div>
        )
    }
}
```

发现当我们点击page2的时候，变化如下：

![](media/15315489426581/15315603121493.jpg)

当点击的page2时候，颜色发生改变，而点击page1和page3的时候，不变,是因为我们在NavLink设置了：


```
activeStyle={{
    fontWeight: 'bold',
    color: 'red'
}}
```

关于NavLink用法, 访问[文档](https://reacttraining.com/react-router/web/api/NavLink)


import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Switch, Route, Link, NavLink} from 'react-router-dom'
import {PageOne, PageTwo, PageThree, Content} from './page'


class Index extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
                {this.props.children}
                <Link to="/page1">页面1</Link>
                <br/>
                <NavLink
                    to="/page2"
                    activeStyle={{
                        fontWeight: 'bold',
                        color: 'red'
                    }}
                >页面2</NavLink>
                <br/>
                <Link to="/page3">页面3</Link>
            </div>
        )
    }
}

ReactDOM.render(
    <Router>
        <Index>

        </Index>
    </Router>,
    document.getElementById('root')
);


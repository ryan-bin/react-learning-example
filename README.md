## React 学习指南

`最好的学习资料: https://reactjs.org/`

`第二好的学习资料,Github Topic: https://github.com/topics/react`


## 脚手架

* [ ] [Jquery-webpack4 脚手架(包含参考项目) ](./boilerplate/js-template)


## 练习

### 基本功脑图

* [ ] [Sass 基本概念脑图](./doc/mindmap/Sass.png)


### React基础部分

* [ ] react-hello-world  [react-hello-world](./react-hello-world/react-hello-world.md)
* [ ] react-basic-jsx  [react-basic-jsx](./react-basic-jsx/react-basic-jsx.md)
* [ ] react-basic-render  [react-basic-render](./react-basic-render/react-basic-render.md)
* [ ] react-basic-react-components-and-props   [react-basic-react-components-and-props](./react-basic-react-components-and-props/react-basic-react-components-and-props.md)
* [ ] react-basic-react-state-and-lifecycle  [react-basic-react-state-and-lifecycle](./react-basic-react-state-and-lifecycle/README.md)
* [ ] react-basic-react-handling-events  [react-basic-react-handling-events](./react-basic-react-handling-events/react-basic-react-handling-events.md)
* [ ] react-basic-react-conditional-rendering  [react-basic-react-conditional-rendering](./react-basic-react-conditional-rendering/react-basic-react-conditional-rendering.md)
* [ ] react-basic-react-lists-and-keys  [react-basic-react-lists-and-keys](./react-basic-react-lists-and-keys/react-basic-react-lists-and-keys.md)
* [ ] react-basic-forms   [react-basic-forms ](./react-basic-forms/react-basic-react-forms.md)
* [ ] react-basic-react-lifting-state-up  [react-basic-react-lifting-state-up](./react-basic-react-lifting-state-up/README.md)
* [ ] react-basic-react-composition-vs-inheritancea  [react-basic-react-composition-vs-inheritancea](./react-basic-react-composition-vs-inheritancea/README.md)

### Redux基础部分

* [ ] redux-hello-world  [redux-hello-world](./redux-hello-world)

### React-Redux基础部分

`最好的学习资料: https://reacttraining.com/react-router/`

* [ ] React-Redux-hello-world  [React-Redux-hello-world](./react-redux-hello-world/README.md)
* [ ] React-Redux 中link 和 NavLink  [React-Redux 中link 和 NavLink](./react-redux-link-navlink/README.md )
* [ ] React-Redux-Router  [react-redux-router](./react-redux-router/README.md)

## React 技能图

![](doc/15311934024570.png)
[Github链接](https://github.com/adam-golab/react-developer-roadmap)


## 工具使用
* [ ] webstorm 快速react项目  [webstorm 快速react项目](./doc/webstorm.md)

## 脚手架

* [ ]  [Jquery webpack4 脚手架](./boilerplate/js-template)

## 资料

### 基础资料

[JavaScript开发文档](https://developer.mozilla.org/en-US/docs/Web/JavaScript)

[CSS 布局基础](http://zh.learnlayout.com/)

[CSS 布局大全](https://github.com/Sweet-KK/css-layout)

[JavaScript权威规范](https://github.com/airbnb/javascript?utm_source=gold_browser_extension)

### React资料

[React.js 小书](http://huziketang.mangojuice.top/books/react/)
[awesome-react项目](https://github.com/enaqx/awesome-react)

### NodeJS资料

[一起学NodeJs](https://github.com/nswbmw/N-blog)

### React 项目资料--工具篇

[react项目模版-react-boilerplate ](https://github.com/react-boilerplate/react-boilerplate)

[完美使用 React, Redux, and React-Router！超好用的脚手架](https://github.com/bodyno/react-starter-kit)

[Catalog of React Components & Libraries ](https://github.com/brillout/awesome-react-components)

[a collection of simple demos of React.js](https://github.com/ruanyf/react-demos)

[Light weight templates for react ](https://github.com/wix/react-templates)

[Render Markdown as React components ](https://github.com/rexxars/react-markdown)

[A collection of components for React, base on bootstrap 4.0.](https://github.com/Lobos/react-ui)

[Converts HTML pages into React components ](https://github.com/roman01la/html-to-react-components)

[ 一个简单的 echarts(v3.0 & v4.0) 的 react 封装。](https://github.com/hustcc/echarts-for-react)

[A data visualization framework combining React & D3](https://github.com/emeeks/semiotic)

[D3 Components for React](https://github.com/codesuki/react-d3-components)

[React Form -Simple, powerful, highly composable forms in React ](https://github.com/react-tools/react-form)

[best-practices-included universal frontend starter kit](https://github.com/olegakbarov/react-redux-starter-kit)

[美观易用的React富文本编辑器，基于draft-js开发](https://github.com/margox/braft-editor)

[](https://github.com/gothinkster/realworld)

### React 项目资料--项目篇

[一个react+redux+webpack+ES6+antd的SPA的后台管理底层框架](https://github.com/duxianwei520/react)

[一个 react + redux 的完整项目 和 个人总结](https://github.com/bailicangdu/react-pxq)

[A frontend framework for building admin SPAs on top of REST services, using React and Material Design](https://github.com/marmelab/react-admin)

[react 后台管理系统解决方案](https://github.com/yezihaohao/react-admin)

[Seedstars Labs Base Django React Redux Project](https://github.com/Seedstars/django-react-redux-base)

[ReactJS version of the original AdminLTE dashboard](https://github.com/booleanhunter/ReactJS-AdminLTE)

[Misago is fully featured forum application written in Python and ES6, powered by Django and React.js](https://github.com/rafalp/Misago)

[使用 Python Flask + SQLAchemy + Celery + Redis + React 开发的用于迅速搭建并使用 WebHook 进行自动化部署和运维，支持 Github / GitLab / Gogs / GitOsc。](https://github.com/NetEaseGame/git-webhook)

[Boilerplate application for a Python/Flask JWT Backend and a Javascript/React/Redux Front-End with Material UI.](https://github.com/dternyak/React-Redux-Flask)

[React+Express+Mongo ->前后端博客网站](https://github.com/Nealyang/React-Express-Blog-Demo)


[用React和Ant Design搭建的一个通用管理后台](https://github.com/jiangxy/react-antd-admin)

[一个综合练习项目，前端技术栈：react全家桶，antd-mobile](https://github.com/capslocktao/react-music-webapp)

[一个react得demo/脚手架项目，包含react16+redux+antd+webpack4+react-router4+sass/less+axios+immutable+proxy技术栈](https://github.com/aiyuekuang/react_home)


[模仿知乎界面的一个简单React demo](https://github.com/tsrot/react-zhihu)

[webpack打包react项目](https://github.com/xulayen/webpack-for-react)

[bird前端项目，基于react、antd、antd-admin，封装常用数据组件，细粒度权限解决方案](https://github.com/liuxx001/bird-front)

[云生活超市后台管理系统——React后台项目：react + react-router4 + redux + antd + axios + sass。](https://github.com/walljs/cms_community_e_commerce)








# react-basic-jsx

本篇内容：

* [ ] 了解jsx语法


为了节省初始化，在[react-hello-world](./react-hello-world) 的基础上。
项目目录名字react-hello-world改为react-basic-jsx，同时`package.json`中的name 改成：react-basic-jsx， 其他不变。

## 项目环境配置

* 安装jsx需要的环境

```
yarn add -D babel-preset-react babel-core babel-loader
```

* webpack.config.js增加配置：

```json
module.exports = {
    entry: __dirname + "/index.js",//已多次提及的唯一入口文件
    output: {
        path: __dirname ,//打包后的文件存放的地方
        filename: "bundle.js"//打包后输出文件的文件名
    },
    module: {
        rules: [
            {
                test: /\.js?$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                query: {
                    presets: ['react']
                }
            }
        ]
    }
}
```

* 增加 .babelrc 文件,内容如下：

```
{
  "presets": ["react"]
}
```

* 同时修改index.js 为：

```json
import React from 'react';
import ReactDOM from 'react-dom';

function formatName(user) {
    return user.firstName + ' ' + user.lastName;
}

const user = {
    firstName: 'Harper',
    lastName: 'Perez'
};

const element = (
    <h1>
        Hello, {formatName(user)}!
    </h1>
);

ReactDOM.render(
    element,
    document.getElementById('root')
);
```
    
然后运行：
```
yarn server
```

界面：
![](media/15310319617478/15310397623665.jpg)


## 小结
根据上面环境可以把[官方的jsx文档](https://reactjs.org/docs/introducing-jsx.html)进行实验一遍


1、jsx是JavaScript的语法扩展，主要提供模板语言，它有JavaScript的全部功能。

2、jsx 中的 {} 可以传入一个js的对象(函数、普通对象等)

3、JSX表示对象,下面三者可以达到相同的效果：

```javascipt
const element = (
  <h1 className="greeting">
    Hello, world!
  </h1>
);
```

```javascipt
const element = React.createElement(
  'h1',
  {className: 'greeting'},
  'Hello, world!'
);
```

```javascipt
// Note: this structure is simplified
const element = {
  type: 'h1',
  props: {
    className: 'greeting',
    children: 'Hello, world!'
  }
};
```




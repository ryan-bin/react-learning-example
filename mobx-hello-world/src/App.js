import React, {Component} from 'react';
import {extendObservable} from 'mobx';
import {observer} from 'mobx-react';

class App extends Component {
    constructor() {
        super();

        // extendObservable 确保counter 这个值可以观察
        extendObservable(this, {
            counter: 0,
        })
    }

    onIncrement = () => {
        this.counter++;
    }

    onDecrement = () => {
        this.counter--;
    }

    render() {
        return (
            <div>
                {this.counter}

                <button onClick={this.onIncrement} type="button">Increment</button>
                <button onClick={this.onDecrement} type="button">Decrement</button>
            </div>
        );
    }
}
// observer 确保App 中可观察的值变换之后作出反应(重新渲染)
export default observer(App);

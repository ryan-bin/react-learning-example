# react-basic-react-lists-and-keys

本篇内容：

* [ ] 了解react中lists 和Keys

## lists

lists 就是对 就是对react 中的element 放在一个列表中。比如：

```
const numbers = [1, 2, 3, 4, 5];
const listItems = numbers.map((number) =>
  <li>{number}</li>
);

ReactDOM.render(
  <ul>{listItems}</ul>,
  document.getElementById('root')
);
```
类似于：

```
<ul>
    <li>1</li>
    <li>2</li>
    <li>3</li>
    <li>4</li>
    <li>5</li>
</ul>
```
但是这样会报warning:

```
"Warning: Each child in an array or iterator should have a unique 'key' prop.

Check the top-level render call using <ul>. See https://fb.me/react-warning-keys for more information.
    in li"
```
需要修改如下：

```javascipt
function NumberList(props) {
  const numbers = props.numbers;
  const listItems = numbers.map((number) =>
    <li key={number.toString()}>
      {number}
    </li>
  );
  return (
    <ul>{listItems}</ul>
  );
}

const numbers = [1, 2, 3, 4, 5];
ReactDOM.render(
  <NumberList numbers={numbers} />,
  document.getElementById('root')
);
```

## Keys
键(Keys) 帮助 React 标识哪个项被修改、添加或者移除了。数组中的每一个元素都应该有一个唯一不变的键(Keys)来标识：

```javascipt
const numbers = [1, 2, 3, 4, 5];
const listItems = numbers.map((number) =>
  <li key={number.toString()}>
    {number}
  </li>
);
```

选择Keys的最佳方法是使用在其兄弟姐妹中唯一标识列表项的字符串。大多数情况下，您会使用数据中的ID作为键：

```javascipt
const todoItems = todos.map((todo) =>
  <li key={todo.id}>
    {todo.text}
  </li>
);
```

当您没有渲染项目的稳定ID时，您可以使用项目索引值作为关键作为最后的选择：

```javascipt
const todoItems = todos.map((todo, index) =>
  // Only do this if items have no stable IDs
  <li key={index}>
    {todo.text}
  </li>
);
```

如果项目的顺序可能会发生变化，我们建议不要使用密钥索引。这可能会对性能产生负面影响，并可能导致组件状态出现问题。查看Robin Pokorny的文章，[深入解释使用索引作为关键的负面影响](https://medium.com/@robinpokorny/index-as-a-key-is-an-anti-pattern-e0349aece318)。如果您选择不为列表项分配显式键，则React将默认使用索引作为键。

这里有一个[深入的解释](https://reactjs.org/docs/reconciliation.html#recursing-on-children)，如果你有兴趣了解更多，[为什么需要Keys](https://reactjs.org/docs/reconciliation.html#recursing-on-children)。

## 使用Key提取组件

Key在上下文数组中才有具体的意义，
例如，如果你提取 一个 ListItem 组件，应该把 key 放置在数组处理的 <ListItem /> 元素中，不能放在 ListItem 组件自身中的 <li> 根元素上。

例子：错误的 key 用法：

```javascipt
function ListItem(props) {
  const value = props.value;
  return (
    // Wrong! There is no need to specify the key here:
    <li key={value.toString()}>
      {value}
    </li>
  );
}

function NumberList(props) {
  const numbers = props.numbers;
  const listItems = numbers.map((number) =>
    // Wrong! The key should have been specified here:
    <ListItem value={number} />
  );
  return (
    <ul>
      {listItems}
    </ul>
  );
}

const numbers = [1, 2, 3, 4, 5];
ReactDOM.render(
  <NumberList numbers={numbers} />,
  document.getElementById('root')
);
```

例子：key 应该在这里指定：

```javascipt
function ListItem(props) {
  // 正确！这里不需要指定 key ：
  return <li>{props.value}</li>;
}

function NumberList(props) {
  const numbers = props.numbers;
  const listItems = numbers.map((number) =>
    // 正确！key 应该在这里被指定
    <ListItem key={number.toString()}
              value={number} />

  );
  return (
    <ul>
      {listItems}
    </ul>
  );
}

const numbers = [1, 2, 3, 4, 5];
ReactDOM.render(
  <NumberList numbers={numbers} />,
  document.getElementById('root')
);
```

*记住这句话*： `一个好的经验准则是元素中调用 map() 需要 keys 。`

## keys 在同辈元素中必须是唯一的

在数组中使用的 keys 必须在它们的同辈之间唯一。然而它们并不需要全局唯一。我们可以在操作两个不同数组的时候使用相同的 keys ：

```javascipt
function Blog(props) {
  const sidebar = (
    <ul>
      {props.posts.map((post) =>
        <li key={post.id}>
          {post.title}
        </li>
      )}
    </ul>
  );
  const content = props.posts.map((post) =>
    <div key={post.id}>
      <h3>{post.title}</h3>
      <p>{post.content}</p>
    </div>
  );
  return (
    <div>
      {sidebar}
      <hr />
      {content}
    </div>
  );
}

const posts = [
  {id: 1, title: 'Hello World', content: 'Welcome to learning React!'},
  {id: 2, title: 'Installation', content: 'You can install React from npm.'}
];
ReactDOM.render(
  <Blog posts={posts} />,
  document.getElementById('root')
);
```

键是React的一个内部映射，但其不会传递给组件的内部。如果你需要在组件中使用相同的值，可以明确使用一个不同名字的 prop 传入。

```javascipt
const content = posts.map((post) =>
  <Post
    key={post.id}
    id={post.id}
    title={post.title} />
);
```

上面的例子中， Post 组件可以读取 props.id，但是不能读取 props.key 。

## 在 JSX 中嵌入 map()

在上面的例子中，我们单独声明了一个 listItems 变量，并在 JSX 中引用了该变量：

```javascipt
function NumberList(props) {
  const numbers = props.numbers;
  const listItems = numbers.map((number) =>
    <ListItem key={number.toString()}
              value={number} />

  );
  return (
    <ul>
      {listItems}
    </ul>
  );
}
```

SX允许在大括号中[嵌入任何表达式](https://reactjs.org/docs/introducing-jsx.html#embedding-expressions-in-jsx)，因此可以 内联 map() 结果:

```javascipt
function NumberList(props) {
  const numbers = props.numbers;
  return (
    <ul>
      {numbers.map((number) =>
        <ListItem key={number.toString()}
                  value={number} />

      )}
    </ul>
  );
}
```
有时这可以产生清晰的代码，但是这个风格也可能被滥用。就像在 JavaScript 中，是否有必要提取一个变量以提高程序的可读性，这取决于你。但是记住，如果 map() 体中有太多嵌套，可能是[提取组件](https://reactjs.org/docs/components-and-props.html#extracting-components)的好时机。








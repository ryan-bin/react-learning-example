# react-hello-world

本篇内容：

* [ ] 用yarn 初始化一个项目

## 项目初始化

* 1. 创建项目目录

```
➜ mkdir react-hello-world && cd react-hello-world
```

* 2. yarn 初始化

```
➜  react-hello-world yarn init
yarn init v1.6.0
question name (react-hello-world):
question version (1.0.0):
question description:
question entry point (index.js):
question repository url:
question author:
question license (MIT):
question private:
success Saved package.json
✨  Done in 10.05s.
```
yarn的初始化npm的初始化一样，目的就是提供一个项目的初始化信息包含在`package.json`中.

* 3. 安装React 和 ReactDOM 

React和ReactDOM 是React 的核心.React 包含(`defines and creates the view components`)： React.createElement、 .createClass、 .Component， .PropTypes， .Children这些操作元素的API。ReactDOM包含(`creating and displaying the webpage`) ReactDOM.render、 .unmountComponentAtNode、.findDOMNode这些渲染API

```
yarn add react react-dom
```

```
➜  react-hello-world ls
node_modules package.json yarn.lock
```
此刻多出了`node_modules` 和 `yarn.lock`

`node_modules`是当前项目中所有模块(及其依赖项)的实际仓库.

`yarn.lock` 文件以保留有关模块依赖关系及其版本的信息

更多关于Yarn工具信息请问访问:[官方链接](https://yarnpkg.com/zh-Hans/)

* 4. 安装webpack和webpack-dev-server

webpack-dev-server 和webpack区别在于前者提供了一个web server.给开发提供了一个临时的web服务器。

```
yarn add -D webpack webpack-dev-server
```

`-D` 表示加入开发模块中.

* 5. 创建应用

`index.html`:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>react-learning-example</title>
</head>
<body>

<div id="root"></div>

</body>
<script type="text/javascript" src="bundle.js"></script>
</html>
```

`index.js`

```javascipt
import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.render(
    React.createElement("div", null, "Hello World"), // <div>Hello World</div>
    document.getElementById("root")
);
```

`webpack.config.js`

```javascipt
module.exports = {
    entry: __dirname + "/index.js",//入口文件
    output: {
        path: __dirname ,//打包后的文件存放的地方
        filename: "bundle.js"//打包后输出文件的文件名
    }
}
```


同时`package.json`也增加一些`scripts`:


```json
{
  "name": "react-hello-world",
  "version": "1.0.0",
  "main": "index.js",
  "scripts": {
    "start": "webpack",
    "server": "webpack-dev-server --open"
  },
  "license": "MIT",
  "dependencies": {
    "react": "^16.4.1",
    "react-dom": "^16.4.1"
  },
  "devDependencies": {
    "webpack": "^4.15.1",
    "webpack-cli": "^3.0.8",
    "webpack-dev-server": "^3.1.4"
  }
}
```
项目结构如下：
![](media/15310224923825/15310262827484.jpg)

* 6. 运行项目

直接`yarn server`, 然后访问本地8080端口，得到如下：

![](media/15310224923825/15310264098446.jpg)

或者`yarn start`:

```
➜  react-hello-world yarn start
yarn run v1.6.0
$ webpack
Hash: 719aefae8ecc03f2160f
Version: webpack 4.15.1
Time: 2328ms
Built at: 2018-07-08 13:07:44
    Asset      Size  Chunks             Chunk Names
bundle.js  99.9 KiB       0  [emitted]  main
[14] ./index.js 196 bytes {0} [built]
    + 14 hidden modules

WARNING in configuration
The 'mode' option has not been set, webpack will fallback to 'production' for this value. Set 'mode' option to 'development' or 'production' to enable defaults for each environment.
You can also set it to 'none' to disable any default behavior. Learn more: https://webpack.js.org/concepts/mode/
✨  Done in 4.84s.

```

发现多了一个`bundle.js`
![](media/15310224923825/15310265109875.jpg)

然后直接点击`index.html`让其在浏览器中打开结果如下：



